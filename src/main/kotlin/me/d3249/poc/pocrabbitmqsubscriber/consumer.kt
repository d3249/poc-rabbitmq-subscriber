package me.d3249.poc.pocrabbitmqsubscriber

import org.springframework.amqp.core.AmqpTemplate
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.stereotype.Component

@Component
class RabbitMQConsumer(private val template: AmqpTemplate){

    @RabbitListener(queues = ["testq1"])
    fun receive(message:String) = println("> $message")
}

