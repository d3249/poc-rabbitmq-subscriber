package me.d3249.poc.pocrabbitmqsubscriber

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class PocRabbitmqSubscriberApplication

fun main(args: Array<String>) {
    runApplication<PocRabbitmqSubscriberApplication>(*args)
}
